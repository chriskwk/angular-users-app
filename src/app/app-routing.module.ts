import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsersListComponents } from './users/list/userslist.component';
const routes: Routes = [

  { path: 'users', component: UsersListComponents },
  { path: 'users', loadChildren: () => import('./users/users.module').then(m => m.UsersModule) },

  { path: '**', redirectTo: 'users' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
