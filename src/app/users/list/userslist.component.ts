import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { users } from '../../../models';

@Component({
  selector: 'users-users',
  styleUrls: ['./userslist.component.scss'],
  templateUrl: './userslist.component.html'
})
export class UsersListComponents{

  usersList: users[];

    constructor
    (
      private router: Router,
      private route: ActivatedRoute,
    ) {
      this.usersList = [];
    }

    ngOnInit() {
      const records = localStorage.getItem('usersList');
      if (records !== null) {
        this.usersList = JSON.parse(records);
      }
      
    }

    AddNew(){
      this.router.navigate(['/users/details']);
    }

    editClick(e) {
      this.router.navigate(['/users/details', { id: e }], { relativeTo: this.route });
    }

    //Delete Function
    deleteClick(id:any){
      const oldRecords = localStorage.getItem('usersList');
      if (oldRecords != null){
        const usersList = JSON.parse(oldRecords);
        usersList.splice(usersList.findIndex((a:any) => a.id == id),1);
        localStorage.setItem('usersList',JSON.stringify(usersList));
      }
      const records = localStorage.getItem('usersList');
      if (records != null){
        this.usersList = JSON.parse(records);
      }
    }  
}
