import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersDetailsComponent } from './details/details.component'
import { UsersListComponents } from './list/userslist.component'

const routes: Routes = [
     //Users Routing
     {
      path: 'users',
      component: UsersListComponents},
      {
      path: 'details',
      component: UsersDetailsComponent},
      {
      path: 'details/:id',
      component: UsersDetailsComponent}
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule {}
