import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, UrlSerializer } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AlertService } from '../../../services/alert.service';
import { users } from '../../../models/users'
@Component({
  selector: 'users-usersList',
  styleUrls: ['./details.component.scss'],
  templateUrl: './details.component.html',
})
export class UsersDetailsComponent implements OnInit {

  users : users;
  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  loading = false;
  submitted = false;

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private alertService: AlertService
  ) {
    this.users = new users();
    this.route.params.subscribe((res) => {
      this.users.id = res['id']
    });
  }

  ngOnInit() {
      this.id = this.route.snapshot.params['id'];
      this.form = this.formBuilder.group({
          id: [],
          firstName: ['', Validators.required],
          lastName: ['', Validators.required],
          designation: ['', Validators.required],
          salary: ['', Validators.required],
          shortbio: ['', Validators.required],
          dateofbirth: ['', Validators.required]
      });

      if(this.id){
        this.getDetails();
      }
}

// Provide new ID for User
  getNewUserId(){
    const oldRecords = localStorage.getItem('usersList');
    if (oldRecords !== null){
      const usersList = JSON.parse(oldRecords);
      return usersList.length + 1;
    } else {
      return 1;
    }
  }

  //Submit Form
  onSubmit(){

    this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }
    
    const latestId = this.getNewUserId();
    this.form.value.id = latestId;
    const oldRecords = localStorage.getItem('usersList');

    if(this.id){
      const oldRecords = localStorage.getItem('usersList');
      if (oldRecords !== null){
        const usersList = JSON.parse (oldRecords);
        usersList.splice(usersList.findIndex((a:any) => a.id == this.users.id),1);
        usersList.push (this.form.value);
        localStorage.setItem('usersList',JSON.stringify(usersList));
        this.goToUsersList();
      }
    } 
    else {
    if (oldRecords !== null) {
      const usersList = JSON.parse(oldRecords);
      usersList.push(this.form.value);
      localStorage.setItem('usersList', JSON.stringify(usersList));
      this.goToUsersList();
    } else {
      const userArr : any[]=[];
      userArr.push(this.form.value);
      localStorage.setItem('usersList', JSON.stringify(userArr));
      this.goToUsersList();
    }
  }
}

//Patch Form Values on Edit
getDetails(){
  const oldRecords = localStorage.getItem('usersList');
    if (oldRecords != null){
      const usersList = JSON.parse(oldRecords);
      const currentUser = usersList.find(m => m.id == this.users.id);
      this.form.patchValue({
        firstName: currentUser.firstName,
        lastName: currentUser.lastName,
        salary: currentUser.salary,
        designation: currentUser.designation,
        shortbio: currentUser.shortbio,
        dateofbirth: currentUser.dateofbirth,
      })
    }
}

  get f() 
  { 
    return this.form.controls; 
  }

  //Re-route to users Listing Page
  goToUsersList(){
    this.router.navigate(['/users']);
  }

}

