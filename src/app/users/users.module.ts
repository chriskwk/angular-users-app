import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { UsersDetailsComponent } from './details/details.component'
import { UsersListComponents } from './list/userslist.component'

import { UsersRoutingModule } from './users-routing.module';
import { MatTableModule } from '@angular/material/table';

import { NgbDropdownModule, NgbTooltipModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap'

import { NgbPaginationModule, NgbTypeaheadModule, NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule} from 'ngx-mask'
@NgModule({
  declarations: [
    UsersDetailsComponent,
    UsersListComponents
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UsersRoutingModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbNavModule,
    NgbPaginationModule,
    NgbCollapseModule,
    NgbTypeaheadModule,
    MatTableModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
  ]
})
export class UsersModule { }
